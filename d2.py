#!/usr/bin/env python3
from queue import Queue
from threading import Thread, Condition
from pathlib import Path
from time import sleep
import collections
import os
import shutil
import logging

from XRootD import client as xrd_client

try:
    from alienpy import alien
except:
    try:
        from xjalienfs import alien
    except:
        print("Can't load alienpy, exiting...")
        sys.exit(1)

# from jcachemgr import CacheManager

l = logging.getLogger('main')
l.setLevel(logging.ERROR)

CopyJob = collections.namedtuple('CopyJob', ['base', 'names', 'guids', 'envelopes'])

class DownloadManager:
    def __init__(self, j, workdir, cache, max_workers=2):
        self.max_workers = max_workers
        self.cache = cache
        self.j = j

        self.die = False
        self.in_queue = Queue(1)
        self.out_queue = Queue(1)
        self.workdir = str(Path(workdir).absolute())

        self.t_workers = []
        self.t_scheduler = None
        self.t_committer = None

        self.reset()
        self.cv = Condition()

    def reset(self):
        self.success_jobs = []
        self.failed_jobs = []
        self.n_downloaded = 0

    def fetch(self, shuffle, count):
        self.reset()
        self._start_threads(shuffle, count)

    def make_copy_job(self, tup, names=None):
        base = tup[0]
        guids = list(tup)[1:]

        if names is not None:
            assert len(guids) == len(names)

        envelopes = []

        for g in guids:
            srcs = self._get_envelopes(g) if not self.cache.has_guid(g) else None
            dst = os.path.join(self.workdir, g)
            f = (srcs, dst)
            envelopes.append(f)

        return CopyJob(base, names, guids, envelopes)

    def join(self, timeout=None):
        self.t_scheduler.join()

    def _xrd_get(self, envelopes):
        # TODO: handle status codes!
        # TODO: replica failover here
        c = xrd_client.CopyProcess()

        for j in envelopes:
            srcs, dst = j
            if srcs is not None and len(srcs) > 0:
                c.add_job(srcs[0], dst)
            else:
                # already got this file
                pass

        c.prepare()
        c.run()

        return True

    def _get_envelopes(self, guid):
        envelopes = []

        r = self.j.run(f"access read {guid}", opts="nomsg")
        for er in r['results']:
            file_in_zip = er['url'].split('#')[1] if '#' in er['url'] else None
            zip_param = '&xrdcl.unzip={}'.format(file_in_zip) if file_in_zip is not None else ''
            src = "{url}?authz={envelope}{zip}".format(url=er['url'].split('#')[0], envelope=er['envelope'], zip=zip_param)
            envelopes.append(src)

        return envelopes

    def _scheduler(self, shuffle, requested):
        names = shuffle.columns[1:].to_list()
        tuples = shuffle.to_records(index=False)
        n_scheduled = 0

        for t in tuples:
            if n_scheduled + self.max_workers >= requested:
                break

            cj = self.make_copy_job(t, names)
            self.in_queue.put(cj)
            n_scheduled += 1

        cursor = n_scheduled

        while cursor < len(tuples) and self.n_downloaded < requested:
            with self.cv:
              n_in_flight = n_scheduled - (self.n_downloaded+ len(self.failed_jobs))
              n_to_schedule = requested - self.n_downloaded - n_in_flight

            if n_to_schedule > 0:
                cj = self.make_copy_job(tuples[cursor], names)
                self.in_queue.put(cj)
                n_scheduled += 1
                cursor += 1

            with self.cv:
                self.cv.wait()

        self.in_queue.join()
        self.out_queue.join()

    def _worker(self):
        cj = None

        while True:
            try:
                cj = self.in_queue.get(timeout=10)
                status = self._xrd_get(cj.envelopes)
                self.in_queue.task_done()
                self.out_queue.put((status, cj))
            except Exception as e:
                # nothing to do, queue empty
                pass

    def _committer(self):
        while True:
            success, cj = (None, None)
            try:
                success, cj = self.out_queue.get(timeout=1)
                self._commit_tuple(cj.base, cj.names, cj.guids)

                if success: # and committed
                    self.n_downloaded += 1
                    self.success_jobs.append(cj.base)
                    print(f"Downloaded {self.n_downloaded} - {cj.base}")
                else:
                    print(f"Failed {cj.base}")
                    self.failed_jobs.append(cj.base)

                self.out_queue.task_done()

                with self.cv:
                  self.cv.notify()
            except Exception as e:
                # out queue empty, nothing to do
                # print(e)
                pass

    def _commit_tuple(self, base, names, guids):
        assert len(names) == len(guids)
        something_is_missing = False

        for g in guids:
            if not (self.cache.has_guid(g) or self._tmp_has_guid(g)):
                something_is_missing = True
                break

        if something_is_missing:
            return False
        else:
            for g in guids:
                if self.cache.has_guid(g):
                    continue
                else:
                    assert self._tmp_has_guid(g)
                    src = os.path.join(self.workdir, g)
                    dst = self.cache.make_guid_path(g)
                    Path(os.path.dirname(dst)).mkdir(parents=True, exist_ok=True)
                    shutil.move(src, dst)

            return True

        raise Exception("something went wrong in _commit_tuple()")

    def _start_threads(self, shuffle, count):
        kwargs = {
            "shuffle": shuffle,
            "requested": count,
        }

        self.die = False
        self.t_scheduler = Thread(target=self._scheduler, kwargs=kwargs)
        self.t_committer = Thread(target=self._committer, daemon=True)

        self.t_scheduler.start()
        self.t_committer.start()

        for i in range(self.max_workers):
            t = Thread(target=self._worker, daemon=True)
            t.start()
            self.t_workers.append(t)

    def _tmp_has_guid(self, g):
        return os.path.exists(os.path.join(self.workdir, g))
