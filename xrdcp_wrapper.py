import subprocess
import os

def xrdcp(srcs, dst):
    code = None

    for s in srcs:
        try:
            p = subprocess.run(["xrdcp", s, dst], stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
            code = p.returncode
            break
        except subprocess.CalledProcessError as e:
            os.remove(dst)
            code = p.returncode

    if code is not None and code == 0 and os.path.exists(dst):
        return True
    else:
        return False
